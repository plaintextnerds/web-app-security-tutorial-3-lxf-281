"""i_am_vulnerable URL Configuration."""

from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("", include("bad_sql_practices.urls")),
    path("admin/", admin.site.urls),
]
