from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic

from .models import Question, Choice, Vote


class IndexView(generic.ListView):
    template_name = "index.html"
    context_object_name = "latest_question_list"

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by("-pub_date")[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = "polls/detail.html"


def result(request, pk):
    question = get_object_or_404(Question, pk=pk)
    return render(request, "polls/results.html", {"question": question})


def vote(request, question_id):
    choice = get_object_or_404(Choice, pk=request.POST["choice"])
    choice_question = choice.question
    q = get_object_or_404(Question, pk=question_id)
    if choice_question != q:
        return render(
            request,
            "polls/detail.html",
            {
                "question": q,
                "error_message": "You didn't select a valid choice.",
            },
        )
    choice.votes += 1
    choice.save()
    vote = Vote(email=request.POST["email"], question=q)
    vote.save()
    return HttpResponseRedirect(reverse("bad_sql:results", args=(question_id,)))
